import yaml
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import numpy as np
from PIL import Image
import os
from functools import reduce

from multi_modal_fusion.modalities.raman.raman_loader import RamanLoader
from multi_modal_fusion.modalities.felix.felix_loader import FelixLoader
from multi_modal_fusion.modalities.ultrasound.ultrasound_loader import UltrasoundLoader
from multi_modal_fusion.modalities.nir_interactance.nir_interactance_loader import NIRInteractanceLoader
from multi_modal_fusion.modalities.vnir_interactance.vnir_interactance_loader import VNIRInteractanceLoader
from multi_modal_fusion.modalities.labels.label_loader import LabelLoader


class MultiModalDataset(Dataset):
    def __init__(self, dataset_config_dir, normalize=False, transform=None):
        """
        Args:
            dataset_config_dir (str): Indicates the directory where the config.yaml is stored.
            transform (callable, optional): Optional transform to be applied on the spectrogram images.
        """
        self._all_sensor_datasets = {
                                'raman' : RamanLoader,
                                'felix' : FelixLoader,
                                'ultrasound': UltrasoundLoader,
                                'vnir_interactance': VNIRInteractanceLoader,
                                'nir_interactance': NIRInteractanceLoader
                              }

        self._normalize = normalize
        self._dataset_cfg, self._label_cfg = self._load_config(dataset_config_dir)
        self._dataset_loaders = self._load_modality_loaders()
        self._label_loader = self._load_label_loader()
        self._unique_ids = self._get_ids()

        #print(self._dataset_cfg)
        #print(self._dataset_loaders)
        #self.spec_image_paths = spec_image_paths
        self.transform = transform
   

    # Returns a dictiotnary with sensor name (key) and the corresponding yaml config
    def _load_config(self, dataset_config_dir):
        dataset_config_f = os.path.join(dataset_config_dir, "config.yaml")

        try:
            with open(dataset_config_f, 'r') as file:
                cfg_f = yaml.safe_load(file)

            data_cfg = self._get_modality_config_dict(dataset_config_dir, cfg_f['defaults'])
            label_cfg = self._get_modality_config_dict(dataset_config_dir, cfg_f['labels'])

            return data_cfg, label_cfg

        except FileNotFoundError:
            print(f"File not found: {dataset_config_f}")
            return None, None
        except Exception as e:
            print(f"An error occurred while loading the file: {e}")
            return None, None

    # Builds and return a dictiotnary with sensor name (key) and the corresponding yaml config
    def _get_modality_config_dict(self, dataset_config_dir, sensor_list):
        cfg_dict = {}
        for sensor in sensor_list:
            yaml_f_name = sensor + ".yaml"
            sensor_cfg_f = os.path.join(dataset_config_dir, yaml_f_name)
            if not os.path.exists(sensor_cfg_f):
                print("Yaml does not exist: {sensor_cfg_f})")
            else:
                cfg_dict[sensor] = sensor_cfg_f
        
        return cfg_dict

    def _load_modality_loaders(self):
        data_loader_dict = {}
        for sensor in self._dataset_cfg:
            try:
                loader = self._all_sensor_datasets[sensor]
                cfg_fpath = self._dataset_cfg[sensor]

                with open(cfg_fpath, 'r') as file:
                    loader_cfg = yaml.safe_load(file)

                loader_instance = loader(loader_cfg[sensor], self._normalize) #build the loader instance
                loader_instance.load() #Load the instance with dataset specific variables
                data_loader_dict[sensor] = loader_instance
            except Exception as e:
                print(f"An error occurred while loading the loader for {sensor} data : {e}")
                return None

        return data_loader_dict

    def _load_label_loader(self):
        try:
            cfg_fpath = self._label_cfg["labels"]
            with open(cfg_fpath, 'r') as file:
                loader_cfg = yaml.safe_load(file)

            label_loader = LabelLoader(loader_cfg['labels'])
            label_loader.load()

            return label_loader

        except Exception as e:
            print(f"An error occurred while loading the loader for ground truth label data : {e}")
            return None


    #Returns the ids of items for a given modality, if modality is given
    #else, returns the unique ids common across all the modalities
    def _get_ids(self, modality=None):
        #Deals with the assumption that not all samples could be measured by all sensors
        if modality is None: #intersect the ids from all modalities
            id_set_dict = {}
            for sensor in self._dataset_loaders:
                id_set_dict[sensor] = set(self._dataset_loaders[sensor].get_ids())
        
            # Intersect across all sets in the dictionary
            common_ids = reduce(lambda s1, s2: s1.intersection(s2), id_set_dict.values())
            common_ids_list = sorted(common_ids)
            return common_ids_list
        else: #Modality is given as input
            try:
                return set(self._dataset_loaders[modality].get_ids())
            except Exception as e:
                printf ("Error during loading the ids of {modality} modality : {e}")


    def retrieve_ids(self):
        return self._unique_ids

    def __len__(self, modality=None):

        if modality is None:
            return len(self._unique_ids)
        else:
            return len(self._dataset_loaders[modality])

        pass        

            
    #Extract single data point given the id
    def __getitem__(self, idx):
        default_idx = 1 #if i cant retrieve data, i wil get data from the 1st id
        data_retrieval_successful = True
        items = {}
        try:
            for sensor in self._dataset_loaders:
                loader = self._dataset_loaders[sensor]
                items[sensor] = loader[idx]

                label = self._label_loader[idx]
                #if there is failure to retrieve item from any one of the index of the dataset, ignore the data for that index from all modalities
                if items[sensor] is None or label is None:
                    data_retrieval_successful = False
                    break

            if not data_retrieval_successful:
                items = {}
                for sensor in self._dataset_loaders:
                    loader = self._dataset_loaders[sensor]
                    items[sensor] = loader[default_idx]
                    label = self._label_loader[default_idx]

            return items, label

        except Exception as e:
            print(f"Could not retreive the sensor {sensor} for id {idx}. {e}")

