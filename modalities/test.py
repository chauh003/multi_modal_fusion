from multi_modal_fusion.modalities.dataset import MultiModalDataset
from torch.utils.data import Dataset, DataLoader

dataset = MultiModalDataset("/home/aneesh/sandbox/multi-modal/mangos/multi_modal_fusion/config", normalize=True)
batch_size = 10
# Create dataset and dataloader
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

# Iterate over the DataLoader
#for batch_data, batch_labels in dataloader:
#    print(batch_labels.shape)
    #print(batch_data.shape, batch_labels.shape)
    #print(batch_labels.shape)
    #break 

'''
for data, labels  in dataloader:
    L = [[] for _ in range(batch_size)]  
    for key in data:
        if key != 'ultrasound':
            for i, d in enumerate(data[key]):
                L[i].append(d)
                #print("L[{}] shape : {}".format( i, len(L[i]) ))
    #print(len(L))
    #print(len(L[0][0]), len(L[1][1]))
    #break
'''
#print(len(dataset))

#print(dataset.__len__('felix'))
#print(dataset.__len__('raman'))
#print(dataset.__len__('ultrasound'))
#print(dataset.__len__('nir_interactance'))
#print(dataset.__len__('vnir_interactance'))

#print(dataset.__getitem__(2))#should be present
#print(dataset.__getitem__(288))#Should not be present
#print(dataset.__getitem__(404))#should not be present
#print(dataset.__getitem__(396))#should not be present
#print(dataset.__getitem__(405))#should be present
#print(dataset.__getitem__(490))#should be present
#print(dataset[490])#should be present

item, label = dataset[489]
print(label)
print(item)
