# felix_loader.py
import os
import pandas as pd
import torch

class FelixLoader:

    def __init__(self, config, normalize=False): #Poor coding, last minute changes, normalize is not used anywhere
        self.data_dir = config['path']
        self.filepath = os.path.join(self.data_dir, config['data_f'])

    def load(self):
        try:
            #print(self.filepath)
            self._data_df = pd.read_csv(self.filepath, header=None)

            self._features = self._data_df.iloc[:,1:]
            self._fruit_ids = self._data_df.iloc[:,0]
#             print(self._data_df)
#             print("After set index")
            self._data_df.set_index(0, inplace=True)
#             print(self._data_df)

        except FileNotFoundError:
            print(f"File not found: {self.filepath}")
            return None
        except Exception as e:
            print(f"An error occurred while loading the file: {e}")
            return None

    def mean(self):
        # Calculate mean
        return self._features.mean(axis=0)

    def stdev(self):
        # Calculate mean
        return self._features.std(axis=0)

    def get_features(self):
        return self._features

    def get_ids(self):
        return self._fruit_ids

    def __len__(self):
        return len(self._fruit_ids)

    #Extract single data point given the id
    def get_item(self, idx):
        # Check if the index is out of the bounds of the data list
#         if idx >= len(self._data_df) or idx < -len(self._data_df):
        if idx >= max(self._fruit_ids)+1 or idx < -max(self._fruit_ids)-1: 

            raise IndexError('Index out of bounds \n NOTE: Indexing begins at 1')

        try:
            return torch.from_numpy(self._data_df.loc[idx].to_numpy()).float()

            
        except Exception as e:
            print(f"Could not retreive the feature for id {idx}. {e}")
            return None

    #Extract single data point given the id
    def __getitem__(self, idx):
        return self.get_item(idx)

    @staticmethod
    def type() -> str:
        return "NIR spectra Felix"
