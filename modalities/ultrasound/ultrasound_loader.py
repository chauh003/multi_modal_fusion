#ultrasound_loader.py
#Takes ultrasound signal, and converting the amplitude of the signal to decibels (dB) into a spectrogram,
import os
import yaml
import json
from matplotlib import pyplot as plt
#from scipy.io import loadmat
#import librosa.display
import numpy as np
#import librosa.display
import io
from PIL import Image
import torch
from torchvision import transforms


class UltrasoundLoader:

    def __init__(self, config, normalize=True):
        self.data_dir = config['path']
        self.filepath = os.path.join(self.data_dir, config['data_f'])
        
        self._normalize = normalize
#         if self._normalize: #We are always applying the transform whether normalize is tru or not | poor coding right here
        self._transform = transforms.Compose([ transforms.Lambda(lambda x: x.convert('RGB')),  # Convert RGBA to RGB
                                                transforms.Resize((224, 224)),  # Resize to what ViT takes as input
                                                transforms.ToTensor(),  # Convert to tensor
                                         transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  # Normalize
                                            ])

    def load(self):
        try:
            with open(self.filepath, 'r') as file:
                data_mat_f = json.load(file)

            #self._features = data
            self._features = self._get_data(data_mat_f)
            self._fruit_ids = self._extract_fruit_ids(data_mat_f)
        except FileNotFoundError:
            print(f"File not found: {self.filepath}")
            return None
        except Exception as e:
            print(f"An error occurred while loading the file: {e}")
            return None

    def _get_linux_filename(self, f):
        # Convert to Linux-style path
        return f.replace("\\", "/")

    # Return full  filepath without the extension and without the distance from the probe
    def _get_data(self, data_mat):
        data = {}
        for k in data_mat:
            fname = os.path.join(self.data_dir, data_mat[k])
            fname = self._get_linux_filename(fname)
            fname_wo_ext, _ = os.path.splitext(fname)

            data[k] = fname_wo_ext
        return data


    #the ids are stored as strings, so converting them to int
    def _extract_fruit_ids(self, data):
        fruits_list_str = list(data.keys())
        return [int(idx) for idx in fruits_list_str]


    def get_features(self) -> dict:
        return self._features

    def get_ids(self):
        return self._fruit_ids

    def __len__(self):
        return len(self._fruit_ids)

    # Get the image corresponding to the id = idx
    # We start by taking the data from the signal when the sensor probe was closest
    def get_item(self, idx, dist_idx=1):
        fruit_key = str(idx)
        extension = "_"+str(dist_idx) +".png" 
        try:
            filename_root = self._features[fruit_key]
            image_f = filename_root+extension
            
            #Load the image
            image = Image.open(image_f)

            # Transform image
            if self._transform:
                image = self._transform(image)
            
#             print(image.shape)

            return image
            
        except Exception as e:
            print(f"An error occurred while loading the file: {e}")
            return None


    def __getitem__(self, idx):
        return self.get_item(idx)

    @staticmethod
    def type() -> str:
        return "Ultrasound spectraogram IMEC"
