#label_loader.py
import os
import pandas as pd
import numpy as np
import torch

class LabelLoader:
    def __init__(self, label_config):
        self.label_dir = label_config['path']
        self.filepath = os.path.join(self.label_dir, label_config['data_f'])
        
        self._data_df = None
        self._labels = None
        self._ids = None

        self._sheet_name = label_config['sheet_name']
        self._label_name = label_config['label']
        self._id_name = label_config['id'] #Name of column with fruit ids

    def load(self):
        try:
            #print(self.filepath)
            self._data_df = pd.read_excel(self.filepath, sheet_name=self._sheet_name)
#             self.normalize_labels()

            self._remove_non_numeric()


            self._labels = self._data_df[self._label_name].values
            self._ids = self._data_df[self._id_name].values
            self._data_df.set_index(self._data_df[self._id_name], inplace=True)

        except FileNotFoundError:
            print(f"File not found: {self.filepath}")
            return None
        except Exception as e:
            print(f"An error occurred while loading the file: {e}")

    def normalize_labels(self):
        self._data_df[self._label_name] = (self._data_df[self._label_name] - self._data_df[self._label_name].mean()) / self._data_df[self._label_name].std()
                
        
    def _remove_non_numeric(self):
        self._data_df[self._label_name] = pd.to_numeric(
                                            self._data_df[self._label_name], errors='coerce')

        self._data_df = self._data_df.dropna(subset=[self._label_name])

        return None

    def get_labels(self):
        return self._labels

    def get_ids(self):
        return self._ids

    def __len__(self):
        return len(self._ids)

    #Extract single data point given the id
    def get_item(self, idx):

        # Check if the index is out of the bounds of the data list
#         if idx >= len(self._data_df) or idx < -len(self._data_df): 
        if idx >= max(self._ids)+1 or idx < -max(self._ids)-1: 

            raise IndexError('Index out of bounds \n NOTE: Indexing begins at 1')
       
        try: 
            label = self._data_df.loc[idx][self._label_name]
            label_as_array = np.array(label, dtype=np.float32).reshape(-1)
            return torch.from_numpy(label_as_array).float()
        

        except Exception as e:
            print(f"Could not retreive the label for id {idx}. {e}")
    
        return None

    #Extract single data point given the id
    def __getitem__(self, idx):
        return self.get_item(idx)

    @staticmethod
    def type() -> str:
        return "raman spectra"
