# Important points about the model and the data for the Optuna trial
**Model related**
- OneDFeatureExtractor simplified, with no SE layers and dropout of 0.8
- Gating mechanism has now dropout

**Data related**
- Log_normalized VNIR, NIR, Raman with the option of non-normalized as well (as part of the trials)
- Felix (as is)
- Ultrasound (spectrogram images)




# Follow the following steps to add new sensor
1. create <new_sensor>.yaml in the config directoroty
2. add the <new_sensor> name to config.yaml
3. cd modalities
4. create <new_sensor> directory mkdir <new_sensor>
	cd <new_sensor>
	touch __init__.py # create an empty init file 

	create the loader for the data under the file
	<new_sensor>_loader.py
	
	Optional: Create a "Data explorer.ipynb" to exlore and sanity check the sensor data using this loader


5. Mode to modality parent directoy
	cd ..
   Import the loader in dataset.py, for example:
   	from ultrasound.ultrasound_loader import UltrasoundLoader

6. Extend dataset.py 
	in class MultiModalDataset, another dict item corresponsing to the new sensor to the variable _all_sensor_datasets
	<key>:<loader>
	For example:
	'ultrasound': UltrasoundLoader

	Here key 'ultrasound' should be the same as written in the config.yaml
	Here value 'UltrasoundLoader' should be the same as the imported loader


# To use the library 
- Add the library location to PYTHONPATH
