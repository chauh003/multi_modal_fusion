import torch
import torch.nn as nn
import timm  # For vision transformer

from torch.utils.data import Dataset, DataLoader
from multi_modal_fusion.src.models.limoe_model import MultimodalLiMoE
from multi_modal_fusion.modalities.dataset import MultiModalDataset


#####################################################################
# Testing with some data from my dataloader

#Now just getting a random sample from own dataset and passing it through the MLIMOE
dataset = MultiModalDataset("/home/aneesh/sandbox/multi-modal/mangos/multi_modal_fusion/config")

# Create dataset and dataloader
dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

# Iterate over the DataLoader
for sensor_data, sensor_labels in dataloader:
    try:
        print(sensor_labels)
        print(sensor_data.keys() )
        break
    except:
        continue #Just in case some index failed

#active_sensors = {'felix', 'raman', 'ultrasound'}
one_d_input_dims_list = [sensor_data['felix'].shape[-1], sensor_data['raman'].shape[-1]]
one_d_data_list = [sensor_data['felix'], sensor_data['raman']]

spectrogram = sensor_data['ultrasound']
#print(spectrogram.shape)


# Example parameters initialization
model = MultimodalLiMoE(
    signal_input_dims=one_d_input_dims_list, 
    signal_output_dim=32, 
    num_experts=5, 
    expert_output_dim=128
)


# Ensure the model is in evaluation mode if you're just testing output shapes or inference
model.eval()

# Pass the inputs through the model
with torch.no_grad():  # Ensure no gradients are computed if you're just testing
    output = model(spectrogram, one_d_data_list)

# Print the output shape
print("Output shape:", output.shape)

print(output)

