import torch
import torch.nn as nn
import timm  # For vision transformer

import torch
import torch.nn as nn


import torch
import torch.nn as nn
import torch.nn.functional as F


# incorporating attention by using Squeeze-and-Excitation blocks. 
#These blocks adaptively recalibrate channel-wise feature responses by explicitly modelling interdependencies between channels
#should be valuable for my set of features where there is dependency on one hand, and on the other relevance
class SELayer(nn.Module):
    def __init__(self, channel, reduction=16):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool1d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction, bias=False),
            nn.ReLU(inplace=True),
            nn.Linear(channel // reduction, channel, bias=False),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1)
        return x * y.expand_as(x)

#Uses sqeeze-excitation for attention on regions, normalization and dropout for regularization
class OneDFeatureExtractor(nn.Module):
    def __init__(self, input_dim, output_dim, dropout_rate=0.8):  # Increased dropout rate
        super(OneDFeatureExtractor, self).__init__()

        self.conv1d_1 = nn.Sequential(
            nn.Conv1d(in_channels=input_dim, out_channels=int(output_dim * 0.75), kernel_size=5, padding=2),
            nn.ReLU(),
            #nn.BatchNorm1d(int(output_dim * 0.75)),
            #SELayer(int(output_dim*0.75)),
            nn.Dropout(dropout_rate)
        )

        self.conv1d_2 = nn.Sequential(
            nn.Conv1d(in_channels=int(output_dim * 0.75), out_channels=int(output_dim * 0.5), kernel_size=7, padding=3),
            nn.ReLU(),
            #nn.BatchNorm1d(int(output_dim * 0.5)),
            SELayer(int(output_dim*0.5)),
            nn.Dropout(dropout_rate)
        )

        # Removed one convolutional layer to reduce complexity
        # Simplified the model by not using SELayer in every layer

        self.conv1d_reduce = nn.Conv1d(in_channels=int(output_dim * 0.5), out_channels=output_dim, kernel_size=1)
        self.adaptive_pool = nn.AdaptiveAvgPool1d(1)

    def forward(self, x):
        x = x.view(x.size(0), x.size(1), -1)
        x = self.conv1d_1(x)
        x = self.conv1d_2(x)
        x = self.conv1d_reduce(x)
        x = self.adaptive_pool(x)
        x = torch.flatten(x, 1)
        return x


class OneDFeatureExtractorNoAttention(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(OneDFeatureExtractor, self).__init__()
        
        self.conv1d_1 = nn.Sequential(
            nn.Conv1d(in_channels=input_dim, out_channels=int(output_dim), kernel_size=5, padding=1),
            nn.ReLU(),
            nn.BatchNorm1d(int(output_dim))
        )
        
        self.conv1d_2 = nn.Sequential(
            nn.Conv1d(in_channels=int(output_dim), out_channels=int(output_dim*1.5), kernel_size=11, padding=2),
            nn.ReLU(),
            nn.BatchNorm1d(int(output_dim*1.5))
        )
        
        self.conv1d_3 = nn.Sequential(
            nn.Conv1d(in_channels=int(output_dim*1.5), out_channels=int(output_dim*2), kernel_size=17, padding=3),
            nn.ReLU(),
            nn.BatchNorm1d(int(output_dim*2))
        )

        # Reducing back to the desired output dimension while incorporating features from various scales
        self.conv1d_reduce = nn.Conv1d(in_channels=int(output_dim*2), out_channels=output_dim, kernel_size=1)
        
        self.adaptive_pool = nn.AdaptiveAvgPool1d(1)

    def forward(self, x):
        x = x.view(x.size(0), x.size(1), -1)  # Ensure the input is in the correct shape
        x = self.conv1d_1(x)
        x = self.conv1d_2(x)
        x = self.conv1d_3(x)
        x = self.conv1d_reduce(x)  # Reduce channel dimension back to output_dim
        x = self.adaptive_pool(x)
        x = torch.flatten(x, 1)
        return x


#Added training of the positional embedding layer
class ViTFeatureExtractor(nn.Module):
    def __init__(self, pretrained=True, trainable_layers=3, fine_tune_positional_embeddings=False):
        super(ViTFeatureExtractor, self).__init__()
        # Load pre-trained Vision Transformer and remove the classification head
        self.model = timm.create_model('vit_base_patch16_224', pretrained=pretrained)
        self.model.head = nn.Identity()

        # Freeze all parameters first
        for param in self.model.parameters():
            param.requires_grad = False

        # Make the last N layers trainable
        layers = list(self.model.blocks[-trainable_layers:])  # Accessing the last N layers
        for layer in layers:
            for param in layer.parameters():
                param.requires_grad = True

        # Optionally fine-tune positional embeddings
        if fine_tune_positional_embeddings:
            self.model.pos_embed.requires_grad = True

    def forward(self, x):
        return self.model(x)


#vit as the feature extractor, but possibility to fine-tune only some of the layer
class ViTFeatureExtractor_nopositional_embedding_training(nn.Module):
    def __init__(self, pretrained=True, trainable_layers=3):
        super(ViTFeatureExtractor, self).__init__()
        # Load pre-trained Vision Transformer and remove classification
        self.model = timm.create_model('vit_base_patch16_224', pretrained=pretrained)
        #self.model = timm.create_model('vit_small_patch16_224', pretrained=pretrained)
        #self.model = timm.create_model('vit_tiny_patch16_224', pretrained=pretrained)
        self.model.head = nn.Identity()  # Remove original classification head

        # Freeze all layers
        for param in self.model.parameters():
            param.requires_grad = False

        # Make last N layers trainable.
        # Assuming 'trainable_layers' is the number of layers from the end you want to train
        layers = list(self.model.blocks[-trainable_layers:])  # Get the last N layers
        for layer in layers:
            for param in layer.parameters():
                param.requires_grad = True

    def forward(self, x):
        #print("image shape: ", x.shape)
        return self.model(x)



class MultimodalLiMoE(nn.Module):
    def __init__(self, signal_input_dims, signal_output_dim, num_experts, expert_output_dim, trainable_vit_layers=3, fine_tune_vit_positional_embeddings=False, gating_hidden_dim=32, gating_dropout=0.2):
        super(MultimodalLiMoE, self).__init__()
        self.vit_feature_extractor = ViTFeatureExtractor(pretrained=True, trainable_layers=trainable_vit_layers, fine_tune_positional_embeddings=fine_tune_vit_positional_embeddings)

        # This should handle multiple 1D signals
        self.signal_models = nn.ModuleList([OneDFeatureExtractor(input_dim=signal_input_dim, output_dim=signal_output_dim) for signal_input_dim in signal_input_dims])

        # Assuming concatenated features from ViT and Signal Classifier
        combined_feature_dim = self.vit_feature_extractor.model.embed_dim + signal_output_dim * len(signal_input_dims)

        self.experts = nn.ModuleList([nn.Linear(combined_feature_dim, expert_output_dim) for _ in range(num_experts)])
        
        # Enhanced gating mechanism with a hidden layer
        self.gate = nn.Sequential(
            nn.Linear(combined_feature_dim, gating_hidden_dim),
            nn.ReLU(),
            nn.Dropout(p=gating_dropout),
            nn.Linear(gating_hidden_dim, num_experts)
        )

        self.final_classifier = nn.Linear(expert_output_dim, 1)  # For regression

    def forward(self, image, signal_list):
        image_features = self.vit_feature_extractor(image)

        signal_features_list = [model(signal) for model, signal in zip(self.signal_models, signal_list)]
        combined_signal_features = torch.cat(signal_features_list, dim=1)
        
        combined_features = torch.cat((image_features, combined_signal_features), dim=1)

        # Gating mechanism (using softmax on enhanced gate's output)
        gate_outputs = torch.softmax(self.gate(combined_features), dim=1)

        # Expert outputs
        expert_outputs = torch.stack([expert(combined_features) for expert in self.experts], dim=1)

        # Weighted sum of expert outputs based on gate
        weighted_expert_outputs = torch.sum(gate_outputs.unsqueeze(2) * expert_outputs, dim=1)

        return self.final_classifier(weighted_expert_outputs)

