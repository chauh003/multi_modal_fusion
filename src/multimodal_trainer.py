import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import r2_score
import mlflow
from torch.utils.data import DataLoader


class MultiModalTrainer:
    def __init__(self, train_dataset, val_dataset, model, batch_size=3, lr=1e-4, weight_decay=1e-5, epochs=10):
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.model = model
        self.batch_size = batch_size
        self.lr = lr
        self.epochs = epochs
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        #print(self.device)
        self.criterion = nn.MSELoss()  
        #self.optimizer = optim.Adam(self.model.parameters(), lr=self.lr)
        #Since I am freezing some layers, have to make sure that the optimizer receives parameters that require gradients. I filter the model parameters to only include those with requires_grad=True.
        self.optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, self.model.parameters()), lr=lr, weight_decay=weight_decay)

        self.model.to(self.device)


    def train(self):
        train_loader = torch.utils.data.DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True)
        val_loader = torch.utils.data.DataLoader(self.val_dataset, batch_size=self.batch_size)

        # Early stopping criteria
        patience = 5  # Number of epochs to wait after potential overfitting is detected
        best_val_loss = float('inf')
        best_val_r2 = -float('inf') 
        best_epoch = -1
        overfitting_signal_count = 0  # Counter for epochs where overfitting signal is observed

        for epoch in range(self.epochs):
            self.model.train()
            train_loss, train_r2 = self.run_epoch(train_loader, epoch, training=True)

            self.model.eval()
            with torch.no_grad():
                val_loss, val_r2 = self.run_epoch(val_loader, epoch, training=False)

            # Log metrics for each epoch using MLflow
            mlflow.log_metrics({'train_loss': train_loss, 'val_loss': val_loss, 'train_r2': train_r2, 'val_r2': val_r2}, step=epoch)

            print(f"Epoch {epoch}, Train Loss: {train_loss}, Val Loss: {val_loss}, Train-R2: {train_r2}, Val-R2: {val_r2}")

            # Update best validation loss and reset overfitting signal counter if validation loss improves
            #if val_loss < best_val_loss:
            if val_r2 > best_val_r2:
                best_val_loss = val_loss
                best_val_r2 = val_r2
                best_epoch = epoch
                overfitting_signal_count = 0
            else:
                # If validation loss does not improve, check for overfitting signal
                #if train_loss < best_val_loss:
                if train_r2 > 2*best_val_r2: #Checking if training r2 s double than best val2
                    overfitting_signal_count += 1

            # Check if overfitting signal has been observed for a number of epochs exceeding patience
            if overfitting_signal_count > patience:
                print(f"Early stopping triggered. Stopping training after {epoch+1} epochs due to overfitting.")
                # Log metrics for when overfitting occurred
                mlflow.log_metrics({'overfitting_signal_count': overfitting_signal_count, 
                    'best_epoch': best_epoch, 
                    'best_val_loss': best_val_loss, 
                    'best_val_r2_at_best_epoch': best_val_r2, 
                    'final_epoch': epoch, 
                    'final_val_loss': val_loss, 
                    'final_val_r2': val_r2}, step=epoch)
                break

        # Log the model at the best epoch
        mlflow.pytorch.log_model(self.model, "model")

        #return val_loss, val_r2
        return best_val_loss, best_val_r2

    def run_epoch(self, loader, epoch, training):
        total_loss = 0.
        all_preds = []
        all_targets = []
        count = 0
        for data, labels in loader:
            count += 1
            #print(epoch, count, len(labels))
            images = data['ultrasound'].to(self.device)
            modalities = data.keys()
            exclude_keys = ['ultrasound', 'image', 'hsi']
            one_d_modalities = [m for m in modalities if m not in exclude_keys]
            signals = [data[key].to(self.device) for key in one_d_modalities]

            targets = labels.to(self.device)

            if training:
                self.optimizer.zero_grad()

            outputs = self.model(images, signals)
            loss = self.criterion(outputs, targets)
            total_loss += loss.item()

            # Convert outputs and targets to CPU and detach from the computation graph
            # This is necessary for accumulating predictions and targets across all batches
            all_preds.append(outputs.detach().cpu())
            all_targets.append(targets.detach().cpu())

            if training:
                loss.backward()
                self.optimizer.step()

        # Concatenate all predictions and targets to calculate R2 score
        all_preds = torch.cat(all_preds).numpy()
        all_targets = torch.cat(all_targets).numpy()
        r2 = r2_score(all_targets, all_preds)


        average_loss = total_loss / len(loader)
        return average_loss, r2


##################################################################################
