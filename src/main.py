#main.py
'''
This is the primary file to begin the Optuna trials for training the multi_modal_fusion network
'''

import torch
import numpy as np
import argparse

import optuna
import mlflow
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader, Subset
from multi_modal_fusion.src.models.limoe_model import MultimodalLiMoE
from multi_modal_fusion.modalities.dataset import MultiModalDataset

from multi_modal_fusion.src.multimodal_trainer import MultiModalTrainer



# Build stratified sets: Our dataset is hihgly imbalanced towards low firmness

def get_stratified_train_val_test_datasets(dataset, test_size=0.15, val_size=0.2):
    indices = dataset.retrieve_ids()
    label_tensors = []
    valid_indices = []
    for i in indices:
        try:
            _ , label = dataset[i]
            valid_indices.append(i)
            #print(idx)
            label_tensors.append(label)

        except:
            pass
    
    labels = torch.tensor([t.item() for t in label_tensors])

    # Discretize the labels into bins
    num_bins = np.ceil(1 + np.log2(len(labels))) / 2   # Sturges' bin count / 2
    labels_binned = np.digitize(labels, bins=np.linspace(min(labels), max(labels)-2, num=int(num_bins)))

    #Get final test indices
    train_idx, test_idx = train_test_split(valid_indices, test_size=test_size, stratify=labels_binned, random_state=42)


    train_label_tensors = []
    for i in train_idx:
        _, label = dataset[i] 
        train_label_tensors.append(label)

    train_labels = torch.tensor([t.item() for t in train_label_tensors])
    train_labels_binned = np.digitize(train_labels, bins=np.linspace(min(train_labels), max(train_labels)-2, num=int(num_bins)))

    #Get final train and val indices
    train_idx, val_idx = train_test_split(train_idx, test_size=val_size, stratify=train_labels_binned, random_state=42)

    #print(len(train_idx))
    #print(len(val_idx))
    #print(len(test_idx))

    # Create Subset instances for train and test sets
    train_dataset = Subset(dataset, train_idx)
    val_dataset = Subset(dataset, val_idx)
    test_dataset = Subset(dataset, test_idx)

    return train_dataset, val_dataset, test_dataset



#1D signals from sensors have varied size, get those for each sensors
def get_list_of_input_dims_of_one_d_signals(dataset):
    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)
    for sensor_data, sensor_labels in dataloader:
        try:
            exclude_keys = ['ultrasound', 'image', 'hsi']
            one_d_modalities = [m for m in sensor_data.keys() if m not in exclude_keys]
            one_d_input_dims_list = [sensor_data[sensor].shape[-1] for sensor in one_d_modalities]

            return one_d_input_dims_list
        except:
            continue #Just in case some index failed

#Defining the Optuna objective
def objective(trial):
    # Define the hyperparameters to tune
    lr = trial.suggest_float('lr', 1e-7, 1e-2, log=True)
    weight_decay = trial.suggest_float('weight_decay', 1e-10, 1e-3, log=True)
    batch_size = trial.suggest_categorical('batch_size', [4, 8, 16, 32, 64])
    epochs = trial.suggest_int('epochs', 5, 200) 

    signal_output_dim = trial.suggest_int('signal_output_dim', 2, 128)
    num_experts = trial.suggest_int('num_experts', 2, 5)
    expert_output_dim = trial.suggest_categorical('expert_output_dim', [4, 8, 16, 32, 64])
    trainable_vit_layers = trial.suggest_categorical('trainable_vit_layers', [0, 1, 2])
    fine_tune_vit_positional_embeddings = trial.suggest_categorical('fine_tune_vit_positional_embeddings', [True, False])
    gating_hidden_dim = trial.suggest_categorical('gating_hidden_dim', [2, 4, 8, 16, 32, 64])
    gating_dropout = trial.suggest_float('gating_dropout', 0.1, 0.8, log=False)
    
    dataset_normalize = trial.suggest_categorical('dataset_normalize', [True, False])

    dataset = MultiModalDataset("/home/aneesh/sandbox/multi-modal/mangos/multi_modal_fusion/config", normalize=dataset_normalize)

    train_dataset, val_dataset, test_dataset = get_stratified_train_val_test_datasets(dataset, test_size=0.05, val_size=0.2)

    one_d_input_dims_list = get_list_of_input_dims_of_one_d_signals(dataset)

    # Initialize the model and trainer with the current set of hyperparameters
    model = MultimodalLiMoE(
        signal_input_dims=one_d_input_dims_list, #bring inside the function
        signal_output_dim=signal_output_dim,
        num_experts=num_experts,
        expert_output_dim=expert_output_dim,
        trainable_vit_layers=trainable_vit_layers,
        fine_tune_vit_positional_embeddings=fine_tune_vit_positional_embeddings,
        gating_hidden_dim = gating_hidden_dim,
        gating_dropout = gating_dropout
        )

    trainer = MultiModalTrainer(train_dataset, val_dataset, model, batch_size=batch_size, lr=lr, weight_decay=weight_decay, epochs=epochs)

    # Start an MLflow run for logging
        # Start an MLflow run for logging
    with mlflow.start_run(run_name=f'Trial_{trial.number}'):
        mlflow.log_params(trial.params)

        final_val_loss, final_val_r2 = trainer.train() #check if i use best or final returning from train() 

        # Log the metrics
        mlflow.log_metrics({'final_val_loss': final_val_loss, 'final_val_r2': final_val_r2})

    # Return the metric to minimize
    #return final_val_loss
    return final_val_r2




def main(experiment_name):

    print(f"Running experiment: {experiment_name}")
    # Set the MLflow experiment name
    mlflow.set_experiment(experiment_name)

    #study = optuna.create_study(direction='minimize')
    study = optuna.create_study(direction='maximize')
    study.optimize(objective, n_trials=300)  # Specify the number of trials

    print("Best hyperparameters: ", study.best_params)
    mlflow.log_params(study.best_params)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Provide the name of the experiment you want to start with Optuna to train the network.')
    parser.add_argument('experiment_name', type=str, help='The name of the Optuna experiment to run.')

    args = parser.parse_args()
    print("Args: ", args)

    main(args.experiment_name)

